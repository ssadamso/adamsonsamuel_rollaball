﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    //creates array for vector3s for positions
    public Vector3[] positions;
    // Start is called before the first frame update
    void Start()
    {
        //Set position to one of the four presets
        int randomNumber = Random.Range(0, positions.Length);
        transform.position = positions[randomNumber];
    }

    // Update is called once per frame
    void Update()
    {
        //rotates capsules constantly
        transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime);
    }

}

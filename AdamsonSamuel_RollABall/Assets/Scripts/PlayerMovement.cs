﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    private bool isBig;
    private float bigTimer;

    void Start ()
    {
        //Declare rb, set count to 0, and make wintext blank
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void Update()
    //set scale back to normal after a certain amount of time after picking up the powerup
    {
        if(isBig == true)
        {
            bigTimer -= Time.deltaTime;
            if(bigTimer <= 0f)
            {
                isBig = false;
                transform.localScale = new Vector3(1, 1, 1);
            }
        }
    }
    void FixedUpdate()
    {
        //movement on WASD
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        //player collects pickups
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        //player collects powerups and scales up, triggers countdown in update
        if (other.gameObject.CompareTag("Powerup"))
        {
            other.gameObject.SetActive(false);
            transform.localScale = new Vector3(2, 2, 2);
            isBig = true;
            bigTimer = 5f;
        }
    }

    void SetCountText ()
    {
        //win text if all boxes are collected
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }
}